﻿using System.Collections.Generic;

namespace Lib.Shared
{
    /// <summary>
    /// Класс для работы с конфигурациями
    /// </summary>
    public abstract class BaseConfig
    {
        /// <summary>
        /// Словарь с данными
        /// </summary>
        Dictionary<string, ConfigItem> data = new Dictionary<string, ConfigItem>();
        
        protected BaseConfig() { }
        protected BaseConfig(List<KeyValuePair<string, string>> parsed) {
            foreach (var pair in parsed)
            {
                if (!data.ContainsKey(pair.Key))
                {
                    data.Add(pair.Key, new ConfigItem(pair.Value));
                }
                else
                {
                    data[pair.Key] = new ConfigItem(pair.Value);
                }
            }
        }

        /// <summary>
        /// Получить параметр по его ключу. 
        /// </summary>
        public ConfigItem ByKey(string key)
        {
            ConfigItem output = null;
            if(!data.TryGetValue(key, out output))
            {
                Output.Log($"[BaseConfig] Отсутствует ключ {key}");
            }
            return output;
        }
    }
}
