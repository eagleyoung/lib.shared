﻿using System;
using System.Runtime.CompilerServices;

namespace Lib.Shared
{
    /// <summary>
    /// Стандартный вывод данных
    /// </summary>
    public static class Output
    {      
        /// <summary>
        /// Событие, вызываемое при очередном выводе
        /// </summary>
        public static event Action<OutputData> Traced = delegate { };
        static void OnTraced(OutputData data)
        {
            Traced?.Invoke(data);
        }

        /// <summary>
        /// Вывести строку
        /// </summary>
        public static void Log(string text)
        {
            var data = new OutputData(text);
            OnTraced(data);   
        }

        /// <summary>
        /// Вывести строку с дополнительной информацией
        /// </summary>
        public static void LogAdditional(string text, string additional)
        {
            var data = new OutputData(text, additional);
            OnTraced(data);
        }

        /// <summary>
        /// Вывести ошибку
        /// </summary>
        public static void Error(string text)
        {
            var data = new OutputData(text, OutputData.Variants.Error);
            OnTraced(data);
        }

        /// <summary>
        /// Вывести ошибку с дополнительной информацией
        /// </summary>
        public static void ErrorAdditional(string text, string additional)
        {
            var data = new OutputData(text, OutputData.Variants.Error, additional);
            OnTraced(data);
        }

        /// <summary>
        /// Вывести предупреждение
        /// </summary>
        public static void Warning(string text)
        {
            var data = new OutputData(text, OutputData.Variants.Warning);
            OnTraced(data);
        }

        /// <summary>
        /// Вывести предупреждение с дополнительной информацией
        /// </summary>
        public static void WarningAdditional(string text, string additional)
        {
            var data = new OutputData(text, OutputData.Variants.Warning, additional);
            OnTraced(data);
        }

        /// <summary>
        /// Вывести данные об ошибке в лог со временем и стеком
        /// </summary>
        public static void TraceError(string text, [CallerMemberName] string name = null, [CallerFilePath] string path = null, [CallerLineNumber] int number = -1)
        {
            string key = $"{path}.{name}.{number}";
            TraceErrorInternal(key, text);
        }

        /// <summary>
        /// Вывести данные об исключении в лог со временем и стеком
        /// </summary>
        public static void TraceError(Exception ex, [CallerMemberName] string name = null, [CallerFilePath] string path = null, [CallerLineNumber] int number = -1)
        {
            string key = $"{path}.{name}.{number}";
            TraceErrorInternal(key, ex.GetExceptionData());
        }

        /// <summary>
        /// Вывести данные об исключении с дополнительной информацией в лог со временем и стеком
        /// </summary>
        public static void TraceError(string text, Exception ex, [CallerMemberName] string name = null, [CallerFilePath] string path = null, [CallerLineNumber] int number = -1)
        {
            string key = $"{path}.{name}.{number}";
            TraceErrorInternal(key, $"{text}{Environment.NewLine}{ex.GetExceptionData()}");
        }

        static void TraceErrorInternal(string key, string text)
        {
            var data = $"[{DateTime.Now.Writable()}] {key}{Environment.NewLine}{text}{Environment.NewLine}{Environment.StackTrace}{Environment.NewLine}";
            OnTraced(new OutputData("Ошибка выполнения (см. информацию)", data));
        }
    }
}
