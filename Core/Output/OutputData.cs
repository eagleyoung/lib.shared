﻿namespace Lib.Shared
{
    /// <summary>
    /// Объект представления данных о выводе
    /// </summary>
    public struct OutputData
    {
        /// <summary>
        /// Текст для вывода
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Дополнительная информация
        /// </summary>
        public string Additional { get; }

        /// <summary>
        /// Доступные варианты вывода
        /// </summary>
        public enum Variants { Default, Warning, Error }

        /// <summary>
        /// Текущий вариант вывода
        /// </summary>
        public Variants Variant { get; }

        public OutputData(string text, string additional = null) : this(text, Variants.Default, additional) { }
        public OutputData(string text, Variants variant, string additional = null)
        {
            Text = text;
            Variant = variant;
            Additional = additional;
        }
    }
}
