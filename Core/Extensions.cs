﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace Lib.Shared
{
    /// <summary>
    /// Расширения классов
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Перевести дату в формат, доступный для записи на диск
        /// </summary>
        public static string Writable(this DateTime date)
        {
            return date.ToString("yyyy_MM_dd_HH_mm_ss_fff", CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Перевести формат для записи на диск в дату
        /// </summary>
        public static DateTime FromWritable(this string writableDate)
        {
            return DateTime.ParseExact(writableDate, "yyyy_MM_dd_HH_mm_ss_fff", CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Получить наименование файла без расширения
        /// </summary>
        public static string NameWithoutExtension(this FileInfo info)
        {
            return info.Name.Remove(info.Name.Length - info.Extension.Length);
        }

        /// <summary>
        /// Получить данные об исключении в строковом виде
        /// </summary>
        public static string GetExceptionData(this Exception ex)
        {
            var toLog = ex;
            StringBuilder builder = new StringBuilder();
            string levelOutput = "";
            while (toLog != null)
            {
                builder.AppendFormat("{0}{1}{2}{3}", levelOutput, toLog.GetType().Name, toLog.Message, Environment.NewLine);
                foreach (var line in toLog.StackTrace.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
                {
                    builder.AppendFormat("{0}{1}{2}", levelOutput, line, Environment.NewLine);
                }

                levelOutput += "-";
                toLog = toLog.InnerException;
            }

            return builder.ToString();
        }

        /// <summary>
        /// Синхронизировать коллекции
        /// </summary>
        public static void Sync<T>(this ICollection<T> source, ICollection<T> target)
        {
            if (target == null) return;
            source.Clear();
            foreach (var item in target)
            {
                source.Add(item);
            }
        }

        /// <summary>
        /// Получить последнее исключение в иерархии
        /// </summary>
        public static T GetLast<T>(this Exception ex)
            where T : Exception
        {
            Exception last = ex;
            while(last.InnerException != null)
            {
                last = last.InnerException;
            }

            return last as T;
        }
    }
}
