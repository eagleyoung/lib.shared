﻿namespace Lib.Shared
{
    /// <summary>
    /// Вспомогательные возможности
    /// </summary>
    public static class SharedHelper
    {
        /// <summary>
        /// Перевести количество байт в мегабайты
        /// </summary>
        public static double BytesToMegabytes(long bytes)
        {
            return (bytes / 1024d) / 1024d;
        }
    }
}
