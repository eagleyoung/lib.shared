﻿namespace Lib.Shared.Encryption
{
    public interface IEncryptorKey
    {
        /// <summary>
        /// Пароль
        /// </summary>
        byte[] Key { get; }

        /// <summary>
        /// Вектор инициализации
        /// </summary>
        byte[] IV { get; }
    }
}
