﻿using System.IO;
using System.Security.Cryptography;

namespace Lib.Shared.Encryption
{
    /// <summary>
    /// Объект для шифровки / расшифровки
    /// </summary>
    public class Encryptor
    {
        /// <summary>
        /// Пароль
        /// </summary>
        byte[] Key { get; }

        /// <summary>
        /// Вектор инициализации
        /// </summary>
        byte[] IV { get; }

        public Encryptor(IEncryptorKey key)
        {
            Key = key.Key;
            IV = key.IV;
        }

        /// <summary>
        /// Зашифровать строку
        /// </summary>
        public byte[] Encrypt(string text)
        {
            byte[] encrypted;

            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return encrypted;
        }

        /// <summary>
        /// Расшифровать строку
        /// </summary>
        public string Decrypt(byte[] cipherText)
        {
            string plaintext = null;

            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }
    }
}